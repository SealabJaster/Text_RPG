import rpg.game;

@safe
void main()
{
    auto game = new Game("test_assets");
    game.start();
}
