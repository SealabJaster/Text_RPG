﻿module rpg.game;

private
{
    import rpg.command, rpg.data.loader, rpg.data.entity, rpg.util.input, rpg.data.room, rpg.data.environment;
}

/++
 + This class contains the main state of the game, as well as the game loop logic.
 + ++/
final class Game
{
    private
    {
        ScriptEnvironment   _env;
        CommandHandler      _commands;
        Loader              _loader;
        EntityPlayer        _player;
        Room                _room;
        bool                _shouldLoop;

        @trusted 
        void askForCommand()
        {
            import std.stdio : writefln;

            writefln(""); // formatting
            auto text = Input.read!string;

            try
            {
                if(!this.commands.processCommandString(text, this))
                    writefln("<Unknown command '%s'>\n", text);
            }
            catch(Exception ex)
            {
                writefln("<ERROR: %s>", ex.msg);
                debug writefln("<STACKTRACE: %s>", ex.info);
            }
        }

        @safe
        EntityPlayer startOrLoadGame()
        {
            import std.stdio : writefln;

            while(true)
            {
                string option = "start";

                switch(option)
                {
                    case "start":
                        auto name = Input.read!string("Enter your name");
                        return new EntityPlayer(name, this);

                    default:
                        writefln("<Invalid option '%s'>\n", option);
                        break;
                }
            }

            assert(false);
        }

        @trusted
        void commandHelp(Command.Args args)
        {
            import std.stdio          : writeln;
            import rpg.util.boxWriter : BoxWriter;
            auto writer = new BoxWriter([BoxWriter.Header("Command",        0.25),
                                         BoxWriter.Header("Description",    0.75)]);

            foreach(command; this.commands.commands.byValue)
                writer.addLine([command.name, command.description]);

            writeln(writer);
        }

        @trusted
        void commandPlayerInfo(Command.Args args)
        {
            import std.stdio : writeln;
            writeln(args.game.player);
        }
    }

    public
    {
        /++
         + Constructs a new `Game` object.
         + 
         + Params:
         +  dataFolder = The path to where the folder with the game's assests is.
         + ++/
        @safe
        this(string dataFolder)
        {
            this._env      = new ScriptEnvironment(this);
            this._commands = new CommandHandler();
            this._loader   = new Loader();

            this.loader.loadFromFolder(dataFolder);

            // Register commands
            this.commands.register(Command("help", "Shows the help table", &this.commandHelp));
            debug this.registerDebugCommands();
        }

        /++
         + Starts the game loop.
         + ++/
        @safe
        void start()
        {
            this._player     = this.startOrLoadGame();
            this._shouldLoop = true;
            while(this._shouldLoop)
            {
                this.askForCommand();
            }
        }

        /++
         + Stops the game loop.
         + 
         + If the game loop isn't running, this function does nothing.
         + ++/
        @safe @nogc
        void stop() nothrow
        {
            this._shouldLoop = false;
        }

        /++
         + Changes the room that the player is currently in.
         + 
         + Params:
         +  room = The `Room` to change to.
         + ++/
        @safe
        void changeRoom(Room room)
        {
            import std.stdio     : writefln;
            import std.exception : enforce;

            enforce(room !is null, "Attempted to change into a null room.");

            if(this._room !is null)
                this._room.unregisterCommands(this.commands);

            this._room = room;
            this.room.registerCommands(this.commands);

            writefln("[You have moved into %s]", room.name);
        }

        /++
         + Returns:
         +  The `EntityPlayer` representing the current player.
         + ++/
        @property @safe @nogc
        inout(EntityPlayer) player() nothrow inout
        out(object)
        {
            assert(object !is null);
        }
        body
        {
            return this._player;
        }

        /++
         + Returns:
         +  The main `CommandHandler` for the game.
         + ++/
        @property @safe @nogc
        inout(CommandHandler) commands() nothrow inout
        out(object)
        {
            assert(object !is null);
        }
        body
        {
            return this._commands;
        }

        /++
         + Returns:
         +  The `Loader` for the game.
         + ++/
        @property @safe @nogc
        inout(Loader) loader() nothrow inout
        out(object)
        {
            assert(object !is null);
        }
        body
        {
            return this._loader;
        }

        /++
         + Returns:
         +  The `Room` that the player is currently in.
         + ++/
        @property @safe @nogc
        inout(Room) room() nothrow inout
        out(object)
        {
            assert(object !is null);
        }
        body
        {
            return this._room;
        }

        /++
         + Returns:
         +  The main `ScriptEnvironment` for the game.
         + ++/
        @property @safe @nogc
        inout(ScriptEnvironment) environment() nothrow inout
        out(object)
        {
            assert(object !is null);
        }
        body
        {
            return this._env;
        }
    }

    // Debug only stuff
    private
    {
        @trusted
        void registerDebugCommands()
        {
            debug with(this.commands)
            {
                register(Command("debug_stop",    "[Debug only] Stops the game loop",                       &this.commandDebugStop));
                register(Command("debug_items",   "[Debug only] Shows all loaded items",                    &this.commandDebugItems));
                register(Command("debug_bag",     "[Debug only] Gives the player a premade inventory",      &this.commandDebugBag));
                register(Command("debug_give",    "[Debug only] Gives the player an item",                  &this.commandDebugGive));
                register(Command("debug_rooms",   "[Debug only] Shows all loaded rooms",                    &this.commandDebugRooms));
                register(Command("debug_change",  "[Debug only] Changes to a specified room",               &this.commandDebugChangeRoom));
                register(Command("debug_player",  "[Debug only] Shows information about the player entity", &this.commandPlayerInfo));
                register(Command("debug_races",   "[Debug only] Shows all loaded races",                    &this.commandDebugRaces));
                register(Command("debug_enemies", "[Debug only] Shows all loaded enemies",                  &this.commandDebugEnemies));
            }
        }

        @safe
        void commandDebugStop(Command.Args args)
        {
            this.stop();
        }

        @trusted
        void commandDebugItems(Command.Args args)
        {
            import std.stdio : writeln;

            foreach(item; this.loader.items.byValue)
                writeln(item, "\n");
        }

        @trusted
        void commandDebugRooms(Command.Args args)
        {
            import std.stdio : writeln;

            foreach(room; this.loader.rooms.byValue)
                writeln(room, "\n");
        }

        @trusted
        void commandDebugRaces(Command.Args args)
        {
            import std.stdio : writeln;

            foreach(race; this.loader.races.byValue)
                writeln(race, "\n");
        }

        @trusted
        void commandDebugEnemies(Command.Args args)
        {
            import std.stdio : writeln;

            foreach(info; this.loader.enemies.byValue)
                writeln(info, "\n");
        }

        @trusted
        void commandDebugBag(Command.Args args)
        {
            import std.stdio          : writeln;
            import rpg.data.inventory : Inventory, Item;

            auto bag = args.game.player.bag;
            bag.add(Inventory.ItemStack(new Item("item1", "Test Item #1", "N/A",                        Item.Type.Throwable, null), 20));
            bag.add(Inventory.ItemStack(new Item("item2", "Test Item #2", "This one has a description", Item.Type.Throwable, null), 1));
        }

        @trusted
        void commandDebugGive(Command.Args args)
        {
            import std.stdio          : writefln;
            import std.format         : format;
            import std.exception      : enforce;
            import rpg.data.inventory : Inventory;

            enforce(args.args.length > 0, "Please specify which item to give yourself.");

            auto item = (args.args[0] in args.game.loader.items);
            enforce(item !is null, format("There is no item with the ID of '%s'", args.args[0]));

            if(args.game.player.bag.add(Inventory.ItemStack(*item, 1)))
                writefln("<INFO: You have been given 1 '%s'>", item.name);
            else
                throw new Exception("Your inventory is full");
        }

        @trusted
        void commandDebugChangeRoom(Command.Args args)
        {
            import std.format    : format;
            import std.exception : enforce;
            
            enforce(args.args.length > 0, "Please specify which room to change to.");

            auto room = (args.args[0] in this.loader.rooms);
            enforce(room !is null, format("There is no room with the ID of '%s'", args.args[0]));

            this.changeRoom(*room);
        }
    }
}