﻿module rpg.util.input;

private
{
    import std.stdio;
    import std.traits : isNumeric;
}

/++
 + A helper class that provides a standard way for different parts of the game to get input from the user.
 + ++/
final static class Input
{
    private static
    {
        @safe
        void writeInputPrompt(string label)
        {
            if(label.length == 0) write("> ");
            else                  write(label, " > ");
        }
    }

    public static
    {
        /++
         + Reads in a value from the user.
         + 
         + Params:
         +  T       = The type to read in. Valid types include `string`, and any built-in numeric type.
         +  label   = The text to prompt the user with. For example a label of "Enter name" would tell the user
         +            "Enter name > ".
         + 
         + Returns:
         +  A `T` read in from the user.
         + ++/
        @trusted
        string read(T = string)(string label = "")
        {
            Input.writeInputPrompt(label);

            return readln()[0..$-1];
        }

        /// ditto
        @trusted
        T read(T)(string label = "")
        if(isNumeric!T)
        {
            import std.conv : to;

            while(true)
            {
                try
                {
                    auto text = Input.read!string(label);
                    return text.to!T;
                }
                catch(Exception ex)
                {
                    writeln("<Please enter a valid number>\n");
                }
            }

            assert(false);
        }
    }
}