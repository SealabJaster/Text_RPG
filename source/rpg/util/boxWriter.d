﻿module rpg.util.boxWriter;

private
{
    import std.stdio;
    import std.exception : enforce;
}

/++
 + The box writer is a helper class to format an ASCII box/table.
 + 
 + Example:
 + ---
 +  auto box = new BoxWriter([BoxWriter.Header("Name",   0.5), 
 +                            BoxWriter.Header("Age",    0.10),
 +                            BoxWriter.Header("Gender", 0.40)]);
 +
 +  //           [Name]                      [Age] [Gender]
 +  box.addLine(["Bradley Chatha",           "17", "Male"]);
 +  box.addLine(["Daniel Crutchley-Bentham", "18", "Bigender"]);
 +  box.addLine(["Joshua Marland",           "18", "His own gender"]);
 +  writeln(box);
 + ---
 + ++/
final class BoxWriter
{
    /++
     + Describes a header for the box.
     + ++/
    struct Header
    {
        /// The name of the header.
        string name;

        /// The percentage of space that values under the header can take up.
        float  percentage;
    }

    private
    {
        Header[]    _headers;
        string[][]  _values;
        const(char) _border = '#';
    }

    public
    {
        /++
         + Creates a new box writer that has the given headers.
         + 
         + Params:
         +  headers = The headers of the box writer.
         + ++/
        @safe
        this(Header[] headers)
        {
            import std.algorithm : all;

            enforce(headers.all!(h => h.name.length > 0),                     "Headers must not have empty names.");
            enforce(headers.all!(h => h.percentage > 0 && h.percentage <= 1), "Headers' percentages must be between 0 and 1");
            this._headers = headers;
        }

        /++
         + Adds a line to display into the box.
         + 
         + Params:
         +  values = The values in the line. Value #0 is under header #0, value #1 under header #1, etc.
         + ++/
        @safe
        void addLine(string[] values)
        {
            import std.format : format;
            enforce(values.length == this._headers.length, format("There are %s headers, so each line needs %s values.",
                                                                  this._headers.length, this._headers.length));

            this._values ~= values;
        }

        /++
         + Formats the box into a human-readable form.
         + ++/
        @safe
        override string toString()
        {
            import std.range : repeat, take, array;
            import std.array : appender;
            import std.conv  : to, roundTo;
            import std.math  : round;

            enum         reserved = 2;      // How many characters per line are reserved (for the borders, for example).
            auto         lineSize = 128;    // How many characters can fit on a single line. Defaults to 128.
            auto         toReturn = appender!(char[]);
            char         space    = ' ';    // Used to create padding.
            string[][]   overflowList;      // Keeps a list of all overflows, for when values don't fit properly.

            // Reserve most of the memory in advance.
            toReturn.reserve(lineSize * (4 + this._values.length));

            // Top border
            toReturn ~= this._border.repeat.take(lineSize);
            toReturn ~= '\n';

            // Header values
            foreach(header; this._headers)
            {
                try
                {
                    toReturn ~= "# ";
                    auto size    = (lineSize.to!float * header.percentage).round - reserved;        // Maximum amount of characters the value can use up.
                    auto padding = ((size / 2) - (header.name.length.to!float / 2)).roundTo!size_t; // Left-padding, for centering the header.

                    toReturn ~= space.repeat.take(padding);
                    toReturn ~= header.name;
                    toReturn ~= space.repeat.take(size.roundTo!size_t - (padding + header.name.length)); // Right-padding, for placing the border character properly.
                }
                catch(Exception ex)
                    throw new Exception("There was an error when calculating header-column size. The percentage for a header is likely too small.");
            }
            toReturn.shrinkTo(toReturn.data.length - 1);
            toReturn ~= "#\n";

            // Middle border
            toReturn ~= this._border.repeat.take(lineSize);
            toReturn ~= '\n';

            // Values
            void writeLine(string[] overflow, string value, size_t i)
            {
                bool hasOverflown;
                auto header = this._headers[i]; // The header this value is being put under.
                auto slice  = value;            // A slice to the part of the value to use.
                auto size   = (lineSize.to!float * header.percentage).roundTo!size_t - reserved; // Maximum amount of characters the value can use.

                // If the value's length is more than it's allowed to use per-line, then register it as an overflow.
                // Overflows get printed onto the next line under.
                if(slice.length > size)
                {
                    slice        = slice[0..size];
                    overflow[i]  = value[size..$];
                    hasOverflown = true;
                }

                // "# " [value] [Right-padding]
                toReturn ~= "# ";
                toReturn ~= slice;
                toReturn ~= space.repeat.take(size - slice.length);

                // Add the overflow to the list, if one happened.
                if(hasOverflown)
                    overflowList ~= overflow.dup;
            }

            string[] overflow;
            overflow.length = this._headers.length;

            foreach(line; this._values)
            {
                // Reset the overflow buffer
                overflow[] = "";

                // Write all the values for this line.
                foreach(i, value; line)
                    writeLine(overflow, value, i);

                // Add the ending border.
                toReturn.shrinkTo(toReturn.data.length - 1);
                toReturn ~= "#\n";

                // If there are any overflows, perform them.
                // A standard for-loop is used, because 'overflowList' may grow in size, so the loop needs to keep checking.
                for(size_t i = 0; i < overflowList.length; i++)
                {
                    auto list = overflowList[i];
                    
                    foreach(i2, value; list)
                    {
                        writeLine(overflow, value, i2);
                        overflow[] = "";
                    }

                    // Add the ending border.
                    toReturn.shrinkTo(toReturn.data.length - 1);
                    toReturn ~= "#\n";
                }
                overflowList.length = 0;

                // Value seperation border.
                toReturn ~= this._border.repeat.take(lineSize);
                toReturn ~= '\n';
            }

            return toReturn.data.idup;
        }
    }
}