﻿module rpg.data.environment;

private
{
    import rpg.game, rpg.data.item, rpg.data.inventory, rpg.data.entity;
    import taggedalgebraic;
}

/++
 + Contains an environment for scripts to modify and make use of.
 + ++/
final class ScriptEnvironment
{
    ///
    union _Values
    {
        string       text;
        long         integer;
        double       floatingPoint;
        bool         boolean;
        typeof(null) isNull;
    }

    ///
    alias Variable = TaggedAlgebraic!_Values;

    private
    {
        Variable[string] _variables;
        Game             _game;
    }

    public
    {
        @safe
        this(Game game)
        {
            assert(game !is null);

            this._game = game;
        }

        /++
         + Sets the value of a variable.
         + 
         + Params:
         +  name  = The name of the variable to set.
         +  value = The value to give the variable.
         + ++/
        @trusted
        void setVar(string name, Variable value)
        {
            this._variables[name] = value;
        }

        /++
         + Gets the value of a variable.
         + 
         + Params:
         +  name        = The name of the variable to get the value of.
         +  default_    = The default value to return if the variable doesn't exist.
         + 
         + Returns:
         +  A *copy* of the value of the variable called `name`.
         + 
         +  If a variable called `name` doesn't exist, then `default_` is returned.
         + ++/
        @trusted
        Variable getVar(string name, Variable default_ = Variable(null))
        {
            auto ptr = (name in this._variables);
            return (ptr is null) ? default_ : *ptr;
        }

        /++
         + Uses an `Item`.
         + 
         + Params:
         +  item      = The item to use.
         +  inventory = The `Inventory` that the `item` belongs to.
         +  entity    = The entity using the item
         + ++/
        @trusted
        void useItem(const Item item, Inventory inventory, Entity entity)
        {
            import std.stdio     : writeln;
            import std.exception : enforce;

            enforce(item      !is null, "Attempted to use a null item");
            enforce(inventory !is null, "Attempted to use a null inventory");

            // Find which slot the item is in.
            auto slot = inventory.findSlotById(item.id);
            assert(!slot.isNull, "Could not find the item inside the given inventory");

            final switch(item.type) with(Item.Type)
            {
                case Useable:
                    item.onUse(this._game, entity);
                    break;

                case Throwable:
                    break;

                case Consumable:
                    item.onUse(this._game, entity);
                    inventory.decrementBySlot(slot);
                    break;

                case Weapon:
                    writeln("<INFO: Cannot use item of type '", item.type, "'>");
                    break;
            }
        }
    }
}

/// Convinience alias
alias ScriptVar = ScriptEnvironment.Variable;