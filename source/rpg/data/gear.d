module rpg.data.gear;

public
{
    import rpg.data.item, rpg.util.sdlgen;
    import sdlang;
}

/++
 +
 + ++/
final class Gear
{
    /// How many different `Item.Type`s can be equipped.
    enum gearCount = countEquippableGear();

    private
    {
        Item[gearCount] _gear;

        @trusted
        static size_t typeToIndex(Item.Type type) pure
        out(index)
        {
            assert(index < Gear.gearCount);
        }
        body
        {
            import std.format    : format;
            import std.exception : enforce;

            enforce((type & Item.EQUIP_FLAG) != 0, format("The item type '%s' is unequippable.", type));

            // First nibble of an equippable can be used as an index.
            // This is because of the simple rule set for what their value can be.
            return (type & 0x0F);
        }
        ///
        unittest
        {
            import std.exception : assertThrown;
            static assert(Item.Type.Weapon == 0x10, "Update this unittest.");

            assertThrown(Gear.typeToIndex(Item.Type.Useable));
            assert(Gear.typeToIndex(Item.Type.Weapon) == 0);
        }
    }

    public
    {
        /++
         + Equips an item.
         +
         + Params:
         +  item = The `Item` to equip, it's `Item.Type` is used to determine what slot it is put in.
         +
         + Returns:
         +  If `item` replaces an item that is already equipped, then the replaced item is returned.
         +
         +  So, if `item` isn't replacing something, `null` is returned.
         +
         +  Otherwise, if `item` is replacing something, that something is returned.
         + ++/
        @safe
        Item equip(Item item)
        {
            auto index    = Gear.typeToIndex(item.type);
            auto toReturn = this._gear[index];

            this._gear[index] = item;

            return toReturn;
        }
        ///
        unittest
        {
            auto item1 = new Item("test_1", "Test Weapon #1", "", Item.Type.Weapon);
            auto item2 = new Item("test_2", "Test Weapon #2", "", Item.Type.Weapon);

            auto gear = new Gear();
            assert(gear.equip(item1) is null); // item1 isn't replacing anything, so null is returned.
            assert(gear.equip(item2) == item1); // item2 is replacing item1, so item1 is returned.
        }

        /++
         + Converts the `Gear` into an SDLang `Tag`.
         +
         + Notes:
         +  `Loader.loadGearFromTag` can be used to load the returned tag back in as a set of `Gear`.
         +
         + Returns:
         +  An SDLang `Tag` of this `Gear`.
         + ++/
        @trusted
        Tag toTag()
        {
            auto sdl = new Tag(null, "gear");

            foreach(item; this.items)
                sdl.add(new Tag(null, "itemRef", [sdlang.Value(item.id)]));

            return sdl;
        }

        /// TODO: Lazy
        @trusted
        override string toString() const
        {
            import std.format       : format;
            import std.algorithm    : each;

            auto str  = "<Debug information for a set of Gear>\n";

            this.items.each!(item => str ~= format("[Item in slot '%s']\n%s\n", item.type, item.toString));

            // Cut off leading \n
            if(str[$-1] == '\n')
                str = str[0..$-1];

            return str;
        }

        /++
         + Returns:
         +  An InputRange going over all of the non-null `Item`s that are equipped.
         + ++/
        @property @safe @nogc
        auto items() nothrow const
        {
            import std.algorithm : filter;

            return this._gear[].filter!(i => i !is null);
        }
        ///
        unittest
        {
            // TODO: Once more equippable item types are implemented, make sure the amount of items in the array
            // is lower than the amount of item types that can be equipped, to make sure the null items are filtered.
            auto gear  = new Gear();
            auto items = [new Item("test_weapon", "Test Weapon", "", Item.Type.Weapon)];


            foreach(item; items)
                gear.equip(item);

            import std.range : walkLength;
            assert(gear.items.walkLength == items.length);
        }
    }

    // gear { itemRef "weapon_id"; itemRef "chest_id"; etc... }
    mixin(describeSdl("SdlData", "gear", SdlField!(Item.SdlReference[])("itemRefs")));
}

// 
@safe @nogc
private uint countEquippableGear() nothrow pure
{
    import std.traits : EnumMembers;

    uint count = 0;
    foreach(member; EnumMembers!(Item.Type))
    {
        if((member & Item.EQUIP_FLAG) != 0)
            count += 1;
    }

    return count;
}