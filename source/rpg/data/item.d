module rpg.data.item;

private
{
    import std.typecons : Nullable, nullable;
    import rpg.data.environment, rpg.game, rpg.util.sdlgen, rpg.data.entity;

    import sdlang, taggedalgebraic;
}

public
{
    import rpg.data.stat;
}

/++
 + Describes an item.
 + ++/
final class Item
{
    /// To make some code easier/less verbose to write, any item `Type` that can be equipped
    /// onto an `Entity` will always have this flag set. Test with `(type & Item.EQUIP_FLAG) != 0`
    enum EQUIP_FLAG = 0x10;

    /// Convinience alias
    alias Stats = Stat[string];

    /++
     + Describes the type of an item.
     + ++/
    enum Type
    {
        /// The item can perform an action when used.
        Useable,

        /// Like a `Useable`, except the item is used up.
        Consumable,

        /// Like a 'Consumable', except only usable while in battle.
        Throwable,

        // Rule for equippable items: Their non-flag nibbles must go in ascending order, 0x10 -> 0x11 -> 0x12, etc.
        // This is again because I'm lazy, and want some other code to be easy to write/expand without mixins/boilerplate
        /// Can be equipped into an entity's weapon slot.
        Weapon = Item.EQUIP_FLAG | 0x00
    }

    private
    {
        string         _id;
        string         _name;
        string         _description;
        const(Type)    _type;
        OnUse.Tagged[] _onUse;
        Stats          _stats;
        Stats          _requiredStats;

        // Inheritence, base classes, basic object-oriented programming? Nah, we don't need none of that!
        // (Kill me please)
    }

    public
    {
        /++
         + Constructs a new item. (Base constructor for all types of items)
         +
         + Params:
         +  id    = The ID of the item. The ID is unique to the item, and is how other parts of the
         +          game reference specific items.
         +  name  = The name of the item.
         +  type  = The type of the item.
         + ++/
        @safe @nogc
        this(string id, string name, string description, const(Type) type) nothrow pure
        {
            this._id            = id;
            this._name          = name;
            this._description   = description;
            this._type          = type;
        }

        /++
         + Constructs a new item. (Constructor for 'Usable' type items)
         +
         + Params:
         +  id    = The ID of the item. The ID is unique to the item, and is how other parts of the
         +          game reference specific items.
         +  name  = The name of the item.
         +  type  = The type of the item.
         +  onUse = An array of any struct included in the `OnUse.Values` union, used to define the behaviour
         +          of the item for when it it used.
         + ++/
        @safe @nogc
        this(string id, string name, string description, const(Type) type, OnUse.Tagged[] onUse) nothrow pure
        {
            this(id, name, description, type);
            this._onUse = onUse;
        }

        /++
         + Constructs a new item. (Constructor for 'Weapon' type items)
         +
         + Params:
         +  id          = The ID of the item. The ID is unique to the item, and is how other parts of the
         +                game reference specific items.
         +  name        = The name of the item.
         +  type        = The type of the item.
         +  onUse       = An array of any struct included in the `OnUse.Values` union, used to define the behaviour
         +                of the weapon for when it it used to attack.
         +  stats       = What stats the item provides when equipped.
         +  required    = What stats are required to equip the item.
         + ++/
        @safe @nogc
        this(string id,    string name,    string description, const(Type) type, OnUse.Tagged[] onUse,
             Stats  stats, Stats  required) nothrow pure
        {
            this(id, name, description, type, onUse);
            this._stats         = stats;
            this._requiredStats = required;
        }

        /++
         + Converts the information about an item into a user-friendly form.
         +
         + Note that this function is intended only for debugging purposes, and the output
         + should be made more pretty for actual usage in the game.
         + ++/
        @trusted
        override string toString() const
        {
            import std.format : format;
            
            auto toReturn = "<Debug info for Item>\n";
            toReturn     ~= format("ID: %s\n",          this.id);
            toReturn     ~= format("Name: %s\n",        this.name);
            toReturn     ~= format("Description: %s\n", this.description);
            toReturn     ~= format("Type: %s\n",        this.type);
            toReturn     ~= format("OnUse: %s\n",       (this._onUse is null) ? "No 'onUse' event" : format("%s", this._onUse));
            toReturn     ~= format("Stats: %s\n",       this.stats);
            toReturn     ~= format("Required: %s\n",    this.stats);

            return toReturn;
        }

        /++
         + Uses the item.
         + ++/
        @trusted
        void onUse(Game game, Entity user) const
        {
            foreach(command; this._onUse)
            {
                final switch(command.kind) with(OnUse.Tagged)
                {
                    case Kind.damage:
                        break;

                    case Kind.print:
                        import std.stdio : writeln;

                        writeln(command.text);
                        break;

                    case Kind.heal:
                        user.onHeal(command.amount);
                        break;
                }
            }
        }

        /++
         + Returns:
         +  The item's name.
         + ++/
        @property @safe @nogc
        string name() nothrow pure inout
        {
            return this._name;
        }

        /++
         + Returns:
         +  The item's description.
         + ++/
        @property @safe @nogc
        string description() nothrow pure inout
        {
            return this._description;
        }

        /++
         + Returns:
         +  The item's id.
         + ++/
        @property @safe @nogc
        string id() nothrow pure inout
        {
            return this._id;
        }

        /++
         + Returns:
         +  The item's `Type`.
         + ++/
        @property @safe @nogc
        Type type() nothrow pure inout
        {
            return this._type;
        }

        /++
         + Returns:
         +  A dictionary describing what stats the item gives to an Entity while equipped.
         + ++/
        @property @safe @nogc
        const(Stat[string]) stats() nothrow pure const
        {
            return this._stats;
        }

        /++
         + Returns:
         +  A dictionary describing what stats an Entity needs to equip the item.
         + ++/
        @property @safe @nogc
        const(Stat[string]) requiredStats() nothrow pure const
        {
            return this._requiredStats;
        }
    }

    //
    mixin(describeSdl("SdlItem_Stats",          "stats",         Field!(Stat.SdlData[])("stats")));
    mixin(describeSdl("SdlItem_Stats_Required", "statsRequired", Field!(Stat.SdlData[])("stats")));

    // The entire item tag.
    mixin(describeSdl("SdlData", "item",
            Field!string                              ("id"),
            Field!(Nullable!string)                   ("name"),
            Field!(Nullable!string)                   ("description"),
            Field!(Item.Type)                         ("type"),
            Field!(Nullable!(OnUse.SdlData))          ("onUse"),
            Field!(Nullable!(SdlItem_Stats))          ("stats"),
            Field!(Nullable!(SdlItem_Stats_Required)) ("statsRequired")
    ));
    ///
    unittest
    {
        auto tag = parseSource(`
        item {
            id   "i_slimeFist"
            name "Slime Fist"
            type "Weapon"

            statsRequired {
                stat "strength" value=5
                stat "inteligence" value=1
            }

            onUse {
                damage 5 type="Physical"
                print "This is some text"
            }
        }`).getTag("item");

        auto item = Item.SdlData.parseTag(tag);

        assert(item.id   == "i_slimeFist");
        assert(item.name == "Slime Fist");
        assert(item.type == Item.Type.Weapon);
        assert(item.description.isNull);
        assert(item.stats.isNull);
        assert(item.statsRequired.stats == [Stat.SdlData("strength", 5), Stat.SdlData("inteligence", 1)]);

        with(OnUse.Tagged)
        {
            assert(item.onUse.commands[0].kind == Kind.damage);
            assert(item.onUse.commands[1].kind == Kind.print);

            assert(item.onUse.commands[0].amount == 5);
            assert(item.onUse.commands[0].type   == OnUse.DamageType.Physical);

            assert(item.onUse.commands[1].text == "This is some text");
        }
    }

    // itemRef "item_id"
    // Used as a way for specific items to be referenced to.
    mixin(describeSdl("SdlReference", "itemRef", SdlValue!string("id")));
}

/++
 +
 + ++/
struct OnUse
{
    /// Describes the type of damage a 'damage' tag may inflict.
    enum DamageType
    {
        ///
        Physical
    }

    // [print "Some text here"]
    mixin(describeSdl("Print", "print", SdlValue!string("text")));

    // [damage 293 type="Physical"/"Magical"]
    mixin(describeSdl("Damage", "damage",
        SdlValue!int("amount"),
        SdlAttribute!DamageType("type")
    ));

    // [heal 40]
    mixin(describeSdl("Heal", "heal", SdlValue!int("amount")));

    /// A union of the different generated SDL structs that can be used inside of an OnUse tag.
    union Values
    {
        Print   print;
        Damage  damage;
        Heal    heal;
    }

    ///
    alias Tagged = TaggedAlgebraic!Values;

    // The entire onUse tag.
    mixin(describeSdl("SdlData", "onUse", Field!(Tagged[])("commands")));
}

/++
 + 
 + ++/
Item.Stats toStats(T)(T sdlData)
{
    Item.Stats stats;

    foreach(k, v; sdlData.stats)
        stats[v.name] = Stat.fromSdl(v);

    return stats;
}