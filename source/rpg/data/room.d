﻿module rpg.data.room;

private
{
    import std.typecons;
    import rpg.command, rpg.util.sdlgen;
    import taggedalgebraic;
    import sdlang;
}

/++
 + Represents a room that the player can enter.
 + ++/
final class Room
{
    private
    {
        string          _id;
        string          _name;
        Command[string] _commands;

        alias ComHandlerCommand = rpg.command.Command;
        @safe
        void doCommand(ComHandlerCommand.Args args)
        {
            import std.exception : enforce;
            import std.format    : stdFormat = format; // Clashes with some private function called 'format' in taggedalgebraic.

            auto command = this._commands[args.command];

            foreach(instruction; command.commands)
            {
                final switch(instruction.kind) with(Command.Tagged)
                {
                    case Kind.changeRoom:
                        auto ptr = (instruction.roomID in args.game.loader.rooms);
                        enforce(ptr !is null, stdFormat("A 'change-room' command in room '%s' tried to change to non-existing room '%s'",
                                                        this.name, instruction.roomID));
                        args.game.changeRoom(*ptr);
                        break;
                }
            }
        }
    }

    public
    {
        /++
         + ++/
        this(string id, string name, Command[string] commands)
        {
            this._id        = id;
            this._name      = name;
            this._commands  = commands;
        }

        /++
         + A function so that the commands in the room can be processed by the given `CommandHandler`.
         + 
         + Notes:
         +  The `Room.unregisterCommands` function is the polar opposite of this function.
         + 
         + Params:
         +  handler = The `CommandHandler` to register the commands with.
         + ++/
        @trusted
        void registerCommands(CommandHandler handler)
        {
            import std.exception : enforce;

            enforce(handler !is null, "A null CommandHandler was passed.");

            foreach(key; this._commands.byKey)
                handler.register(ComHandlerCommand(key, "TODO", &this.doCommand));
        }

        /++
         + Unregisters all commands that were registered with `Room.registerCommands`.
         + 
         + Params:
         +  handler = The `CommandHandler` to use.
         + ++/
        @trusted
        void unregisterCommands(CommandHandler handler)
        {
            import std.exception : enforce;
            
            enforce(handler !is null, "A null CommandHandler was passed.");
            
            foreach(key; this._commands.byKey)
                handler.unregister(key);
        }

        @trusted
        override string toString() const
        {
            import std.format : format;

            auto toReturn  = "<Debug information for a Room>\n";
            toReturn      ~= format("ID: %s\n",       this.id);
            toReturn      ~= format("Name: %s\n",     this.name);
            toReturn      ~= format("Commands: %s\n", this.commands.values);

            return toReturn;
        }

        /++
         + Returns:
         +  The Room's ID
         + ++/
        @property @safe @nogc
        string id() nothrow inout
        {
            return this._id;
        }

        /++
         + Returns:
         +  The Room's name
         + ++/
        @property @safe @nogc
        string name() nothrow inout
        {
            return this._name;
        }

        /++
         + Returns:
         +  The `Room.Commands` that the room contains.
         + ++/
        @property @safe @nogc
        inout(Command[string]) commands() nothrow inout
        {
            return this._commands;
        }
    }

    /++
     + Represents the 'command' tag in a room's SDL file.
     + ++/
    struct Command
    {
        /// changeRoom "Some_Room_ID"
        mixin(describeSdl("ChangeRoom", "changeRoom", SdlValue!string("roomID")));

        ///
        union Values
        {
            /// Commands the game to change the room
            ChangeRoom changeRoom;
        }

        ///
        alias Tagged = TaggedAlgebraic!Values;
        mixin(describeSdl("SdlData", "command",
            SdlValue!string ("name"), 
            Field!(Tagged[])("commands")
        ));

        public
        {
            /// The name of the command.
            string name;

            /// The steps to execute the command.
            Tagged[] commands;
        }
    }

    mixin(describeSdl("SdlData", "room",
        Field!string                ("id"),
        Field!(Nullable!string)     ("name"),
        Field!(Command.SdlData[])   ("commands")
    ));
    ///
    unittest
    {
        auto tag = parseSource(`
        room {
            id "r_Test"
            name "Test Room"

            command "north" {
                changeRoom "r_Test_North"
            }

            command "south" {
                changeRoom "r_Test_South"
            }
        }
        `).getTag("room");

        auto sdl = Room.SdlData.parseTag(tag);

        assert(sdl.id   == "r_Test");
        assert(sdl.name == "Test Room");

        with(Room)
        {
            assert(sdl.commands.length == 2);
            assert(sdl.commands == 
            [
                Command.SdlData("north", [Command.Tagged(Command.ChangeRoom("r_Test_North"))]),
                Command.SdlData("south", [Command.Tagged(Command.ChangeRoom("r_Test_South"))])
            ]);
        }
    }
}