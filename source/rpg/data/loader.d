module rpg.data.loader;

private
{
    import rpg.data.item, rpg.data.room, rpg.data.race, rpg.data.stat, rpg.data.entity, rpg.data.loot, rpg.data.inventory, rpg.data.gear;
    import std.exception : enforce;
    import std.typecons  : Nullable;
    import core.thread   : Fiber;
    import sdlang;
}

/++
 + This class provides the functionality to load in the assets for the game.
 + ++/
final class Loader
{
    private
    {
        struct UnresolvedEnemy
        {
            EntityEnemy.Info             info;
            string                       raceId;
            Nullable!(LootTable.SdlData) loot;
            Nullable!(Gear.SdlData)      gear;
        }

        Item[string]             _items;
        Room[string]             _rooms;
        Race[string]             _races;
        EntityEnemy.Info[string] _enemies;

        UnresolvedEnemy[] _unresolvedEnemies;

        @trusted
        void loadItem(Tag tag)
        {
            // Parse the data
            auto sdlData = Item.SdlData.parseTag(tag);

            // If the name wasn't specified, use the ID as it's name.
            if(sdlData.name.isNull)
                sdlData.name = sdlData.id;

            // Handle the optional stuff.
            auto description    = (sdlData.description.isNull)   ? null            : sdlData.description;
            auto onUse          = (sdlData.onUse.isNull)         ? null            : sdlData.onUse.commands;
            auto stats          = (sdlData.stats.isNull)         ? Item.Stats.init : sdlData.stats.toStats;
            auto statsRequired  = (sdlData.statsRequired.isNull) ? Item.Stats.init : sdlData.statsRequired.toStats;

            // Create the item to register.
            Item item;
            final switch(sdlData.type) with(Item.Type)
            {
                case Weapon:
                    item = new Item(sdlData.id, sdlData.name, description, sdlData.type, onUse, stats, statsRequired);
                    break;

                case Throwable:
                case Consumable:
                case Useable:
                    item = new Item(sdlData.id, sdlData.name, description, sdlData.type, onUse);
                    break;
            }

            this._items[sdlData.id] = item;
        }

        @trusted
        void loadRoom(Tag tag)
        {
            auto sdlData = Room.SdlData.parseTag(tag);

            if(sdlData.name.isNull)
                sdlData.name = sdlData.id;

            Room.Command[string] commands;
            foreach(command; sdlData.commands)
                commands[command.name] = Room.Command(command.name, command.commands);

            this._rooms[sdlData.id] = new Room(sdlData.id, sdlData.name, commands);
        }

        @trusted
        void loadRace(Tag tag)
        {
            Stat[string] toDictionary(Stat.SdlData[] stats)
            {
                Stat[string] toReturn;
                foreach(stat; stats) toReturn[stat.name] = Stat.fromSdl(stat);
                return toReturn;
            }

            auto sdlData = Race.SdlData.parseTag(tag);
            if(sdlData.name.isNull)
                sdlData.name = sdlData.id;

            this._races[sdlData.id] = new Race(sdlData.id, sdlData.name, 
                                               toDictionary(sdlData.baseStats.stats), 
                                               toDictionary(sdlData.statsPerLevel.stats));
        }

        @trusted
        void loadEnemy(Tag tag)
        {
            EntityEnemy.Info info;
            auto sdl = EntityEnemy.SdlData.parseTag(tag);

            info.id          = sdl.id;
            info.name        = (sdl.name.isNull)        ? sdl.id : sdl.name;
            info.description = (sdl.description.isNull) ? "N/A"  : sdl.description;
            
            // Since the enemy's race/items in it's loot table might not have been loaded in yet,
            // we need to resolve everything else at a later phase.
            this._unresolvedEnemies ~= UnresolvedEnemy(info, sdl.raceId, sdl.loot, sdl.gear);
        }

        @trusted
        void resolveDependencies()
        {
            import std.format : format;

            // For enemy info
            foreach(unresolved; this._unresolvedEnemies)
            {
                auto dbg = DebugInfo("Enemy", unresolved.info.id);

                auto info = unresolved.info;
                info.race = this.resolveRace(unresolved.raceId, dbg);

                if(!unresolved.loot.isNull)
                    info.loot = this.resolveLoot(unresolved.loot,   dbg);

                try
                {
                    if(!unresolved.gear.isNull)
                        info.gear = this.loadGearFromSdlData(unresolved.gear);
                }
                catch(Exception ex)
                {
                    throw new Exception(format("For Enemy with ID of '%s', unable to load gear: %s", ex.msg));
                }


                // Then add the enemy to the list
                this._enemies[info.id] = info;
            }

            // Error checking
            assert(this._enemies.length == this._unresolvedEnemies.length);
        }

        /++ Helper stuff used to resolve references to other things. ++/
        struct DebugInfo
        {
            string typeName;
            string id;

            string errorMessage(string otherTypeName, string otherId)
            {
                import std.format : format;
                return format("For %s with ID of '%s', unable resolve reference for a %s with ID of '%s'.",
                                this.typeName, this.id, otherTypeName, otherId);
            }
        }

        Race resolveRace(string id, DebugInfo info)
        {
            auto ptr = (id in this._races);
            enforce(ptr !is null, info.errorMessage("Race", id));

            return *ptr;
        }

        Item resolveItem(string id, DebugInfo info)
        {
            auto ptr = (id in this._items);
            enforce(ptr !is null, info.errorMessage("Item", id));

            return *ptr;
        }

        LootTable resolveLoot(LootTable.SdlData sdl, DebugInfo info)
        {
            auto loot = new LootTable();
            foreach(item; sdl.items)
            {
                auto resolved = resolveItem(item.id, info);
                loot.addGuarenteedDrop(Inventory.ItemStack(resolved, item.amount));
            }

            return loot;
        }
    }

    public
    {
        /++
         + Searches through a given folder (including sub-folders) for any SDL-lang files,
         + and treats them as game asset files.
         + 
         + Exceptions:
         +  `Exception` if `folderPath` does not point to a folder.
         + 
         + Params:
         +  folderPath = The path to the folder to load.
         + ++/
        @trusted
        void loadFromFolder(string folderPath)
        {
            import std.algorithm    : filter;
            import std.path         : isDir, dirEntries, SpanMode;
            import std.file         : exists;
            import std.format       : format;

            enforce(folderPath.exists, format("The path '%s' doesn't exist.", folderPath));
            enforce(folderPath.isDir,  format("The path '%s' does not point to a folder.", folderPath));

            struct File
            {
                string name;
                Tag    tags;
            }

            File[] tags; // An array of all the .sdl files.

            // Go over every *.sdl file in the folder, and read it in.
            foreach(entry; dirEntries(folderPath, "*.sdl", SpanMode.breadth))
                tags ~= File(entry.name, parseFile(entry.name));
                
            // Then, load in all of the data.
            foreach(file; tags)
            {
                foreach(tag; file.tags.tags)
                {
                    try
                    {
                        switch(tag.name)
                        {
                            case "item":
                                this.loadItem(tag);
                                break;

                            case "room":
                                this.loadRoom(tag);
                                break;

                            case "race":
                                this.loadRace(tag);
                                break;

                            case "enemy":
                                this.loadEnemy(tag);
                                break;

                            default:
                                throw new Exception(format("Unknown tag '%s'", tag.name));
                        }
                    }
                    catch(Exception ex)
                    {
                        throw new Exception(format("Unable to load file '%s'\nReason: %s",
                                                   file.name, ex.msg));
                    }
                }
            }

            // Then resolve dependencies.
            this.resolveDependencies();
        }

        // Side note: A public function for loading in a set of gear is provided, because
        // things like save files may include a 'gear' tag, and will require item resolution.
        // Because the Loader doesn't load save files, it has to provide a way for some other code
        // to access the item resolver.
        /++
         + Loads a set of `Gear` from an SDLang `Tag`.
         +
         + Params:
         +  tag = The tag to parse.
         +
         + Returns:
         +  A set of `Gear` parsed from `tag`.
         + ++/
        @trusted
        Gear loadGearFromTag(Tag tag)
        {
            import std.format : format;
            enforce(tag.name == "gear", format("Expected tag with name of 'gear', not '%s'.", tag.name));

            auto sdl = Gear.SdlData.parseTag(tag);

            return this.loadGearFromSdlData(sdl);
        }

        /++
         + TODO: Too lazy
         + ++/
        @trusted
        Gear loadGearFromSdlData(Gear.SdlData sdl)
        {
            auto gear = new Gear();
            auto info = DebugInfo("Gear", "N/A");

            foreach(ref_; sdl.itemRefs)
                gear.equip(this.resolveItem(ref_.id, info));

            return gear;
        }

        /++
         + Returns:
         +  An associative array of all loaded `Items`, where the ID of the items are the keys.
         + ++/
        @property @safe @nogc
        inout(Item[string]) items() nothrow pure inout
        {
            return this._items;
        }

        /++
         + Returns:
         +  All loaded `Rooms`.
         + ++/
        @property @safe @nogc
        inout(Room[string]) rooms() nothrow pure inout
        {
            return this._rooms;
        }

        /++
         + Returns:
         +  All loaded `Race`s.
         + ++/
        @property @safe @nogc
        inout(Race[string]) races() nothrow pure inout
        {
            return this._races;
        }

        /++
         + Returns:
         +  All loaded `EntityEnemy.Info`s, which can be passed to the constructor of `EntityEnemey`.
         + ++/
        @property @safe @nogc
        const(EntityEnemy.Info[string]) enemies() nothrow pure const
        {
            return this._enemies;
        }
    }
}