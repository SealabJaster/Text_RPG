module rpg.data.loot;

private
{
    import rpg.util.sdlgen, rpg.data.item, rpg.data.inventory;
}

/++
 + Provides a way to get a loot list, created from pairs of rules and items.
 + ++/
final class LootTable
{
    // item "item_id" amount=20
    mixin(describeSdl("SdlItem", "item", 
        SdlValue!string ("id"),
        SdlAttribute!int("amount")
    ));

    // loot { item "i_rock" amount=1 }
    mixin(describeSdl("SdlData", "loot",
        SdlField!(SdlItem[])("items") // Items that will always drop.
    ));

    private
    {
        alias ItemStack = Inventory.ItemStack;

        const(ItemStack)[] _guarenteed;
    }

    public
    {
        @trusted
        override string toString() const
        {
            import std.format;
            import std.algorithm : map, joiner;

            string output;

            output ~= "<Debug Information for a LootTable>\n";
            output ~= "[Guarenteed to drop]\n";
            output ~= format("\t%s\n", this._guarenteed.map!(stack => format("(ID:'%s', Amount:%s)", stack.item.id, stack.amount))
                                                       .joiner("\n\t"));

            return output;
        }

        /++
         + Add an item into the loot table that is certain to drop.
         +
         + Params:
         +  stack = Contains the item, as well as the amount of the item to drop.
         + ++/
        @safe
        void addGuarenteedDrop(const Inventory.ItemStack stack) nothrow pure
        {
            this._guarenteed ~= stack;
        }
        ///
        unittest
        {
            auto loot = new LootTable();
            auto item = new Item("i_test", "", "", Item.Type.Useable);

            loot.addGuarenteedDrop(Inventory.ItemStack(item, 8));

            assert(loot.genLoot() == 
            [
                Inventory.ItemStack(item, 8)
            ]);
        }

        /++
         + Generates a list of loot.
         +
         + Returns:
         +  A generated list of `Inventory.ItemStack`s based on the items added to the table.
         + ++/
        @safe
        const(Inventory.ItemStack)[] genLoot()
        {
            // Begin with the guarenteed items.
            const(ItemStack)[] items;
            items.reserve(this._guarenteed.length);

            foreach(stack; this._guarenteed)
                items ~= stack;

            return items;
        }
    }
}