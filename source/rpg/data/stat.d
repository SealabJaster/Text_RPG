﻿module rpg.data.stat;

private
{
    import std.traits : allSatisfy, isFloatingPoint;
    import rpg.util.sdlgen;
    import sdlang;
}

/++
 + Describes a stat for something such as an `Entity` or an `Item`.
 + ++/
struct Stat
{
    // stat "StatName" value=20
    mixin(describeSdl("SdlData", "stat",
        SdlValue!string ("name"),
        SdlAttribute!int("value")
    ));

    public
    {
        /// The name of the stat.
        string name;

        /// The value of the stat.
        int value;

        /++
         + Converts a `Stat.SdlData` struct into a `Stat`.
         + 
         + Params:
         +  data = The data to convert.
         +
         + Returns:
         +  `data` converted into a `Stat`.
         + ++/
        @safe @nogc
        static Stat fromSdl(const Stat.SdlData data) nothrow pure
        {
            return Stat(data.name, data.value);
        }
        ///
        unittest
        {
            auto tag = parseSource(`stat "strength" value=50`).getTag("stat");
            auto sdl = Stat.SdlData.parseTag(tag);

            assert(Stat.fromSdl(sdl) == Stat("strength", 50));
        }

        /++
         + Applies multipliers to the stat's value, and returns the result.
         + 
         + This function exists so things like debuffs/buffs can be factored into the stat's value, without changing
         + the actual value of the stat.
         + 
         + Notes:
         +  The multipliers are additive, as in, all of the multipliers are added together before it's
         +  applied to the value.
         + 
         +  A multiplier total of `0.50` means that the value should be increased by 50%.
         +  A multiplier total of `1.50` means that the value should be increased by 150%.
         +  A multiplier total of `-0.5` means that the value should be decreased by 50%.
         +  etc.
         + 
         + Params:
         +  multipliers = The multipliers to apply. `float`, `float[]`, `double`, and `double[]` are all valid types 
         +                to be passed.
         + 
         + Returns:
         +  The `value` of the stat, with all of the `multipliers` applied.
         + ++/
        @trusted
        int applyMultipliers(F...)(F multipliers)
        if(F.length > 0)
        {
            import std.conv      : to, roundTo;
            import std.algorithm : each;
            import std.traits    : isFloatingPoint;

            // Add all of the multipliers together.
            auto multiplier = 0.0;
            foreach(m; multipliers)
            {
                alias type = typeof(m);
                static if(is(type == float[]) || is(type == double[]))
                {
                    foreach(m2; m)
                        multiplier += m2;
                }
                else static if(isFloatingPoint!type)
                    multiplier += m;
                else
                    static assert(false, "Only floating point numbers, and arrays of floating point numbers can be used.");
            }

            // Then perform the calculation.
            auto floatValue = this.value.to!float;
            auto newValue   = (multiplier >= 0) ? floatValue * (multiplier + 1)            // e.g 20 * (0.5 + 1) == 20 * 1.5 == 30
                                                : floatValue - (floatValue * -multiplier); // e.g 20 - (20 * -(-0.5)) == 20 - (20 * 0.5) == 20 - 10 == 10

            return newValue.roundTo!int;
        }
        ///
        unittest
        {
            auto stat = Stat("Strength", 20);

            assert(stat.applyMultipliers(0.50)  == 30);
            assert(stat.applyMultipliers(-0.50) == 10);

            assert(stat.applyMultipliers(0.8, [0.2, -0.75]) == 25);

            assert(stat.applyMultipliers(0.0) == 20);
        }
    }
}