﻿module rpg.data.inventory;

private
{
    import std.typecons : Nullable;
    import rpg.data.item;
}

/++
 + Represents an inventory of `Item`s.
 + ++/
final class Inventory
{
    /++
     + Represents a stack of `Item`s.
     + ++/
    struct ItemStack
    {
        /// The item
        Item item;

        /// The amount of items there are.
        uint amount;
    }

    private
    {
        ItemStack[] _items;
    }

    public
    {
        /++
         + Creates a new Inventory.
         + 
         + Params:
         +  slots = How many slots the inventory has.
         + ++/
        @safe
        this(size_t slots = 16) nothrow
        {
            this._items.length = slots;
        }

        /++
         + Returns:
         +  The contents of the inventory, in a human-readable form.
         + ++/
        @trusted
        override string toString()
        {
            import std.conv           : to;
            import rpg.util.boxWriter : BoxWriter;

            auto writer = new BoxWriter([BoxWriter.Header("Slot",        0.1),
                                         BoxWriter.Header("Name",        0.3),
                                         BoxWriter.Header("Count",       0.1),
                                         BoxWriter.Header("Description", 0.5)
                                        ]);

            foreach(i, stack; this._items)
            {
                string[] data = new string[4];
                data[0]       = i.to!string;
                data[1]       = (stack.item is null) ? "Empty" : stack.item.name;
                data[2]       = stack.amount.to!string;
                data[3]       = (stack.item is null) ? "" : stack.item.description;

                writer.addLine(data);
            }

            return writer.toString;
        }

        /++
         + Adds a stack of items into the inventory.
         + 
         + Behaviour:
         +  If the item is not already in the inventory, then the next empty slot is used up.
         + 
         +  If the item is already in the inventory, then the `stack`'s amount is added to the existing amount.
         + 
         +  If the inventory is full, and the item is not already in the inventory, then the stack is not added.
         + 
         +  If the given item stack has 0 items in it, then this function does nothing.
         + 
         + Params:
         +  stack = The `ItemStack` to add.
         + 
         + Returns:
         +  `true` if `stack` was succesfully added.
         + 
         +  `false` otherwise.
         + ++/
        @safe @nogc
        bool add(ItemStack stack) nothrow
        {
            if(stack.amount == 0) return false;

            // Reminder: There will be no empty slots between items, so it is safe to always assume
            // that once you hit a single empty slot, then the rest of the slots will be empty.
            foreach(ref slot; this._items)
            {
                if(slot.item is null)
                {
                    slot.item   = stack.item;
                    slot.amount = stack.amount;
                    return true;
                }
                else if(slot.item.id == stack.item.id)
                {
                    slot.amount += stack.amount;
                    return true;
                }
            }

            return false;
        }
        ///
        unittest
        {
            auto bag    = new Inventory(2);
            auto item1  = new Item("item1", "Test Item", "", Item.Type.Throwable, null);
            auto item2  = new Item("item2", "Test Item", "", Item.Type.Throwable, null);
            auto item3  = new Item("item3", "Test Item", "", Item.Type.Throwable, null);

            // Scenario #1, adding an item when there's no other versions of the item in the inventory.
            assert(bag.add(ItemStack(item1, 20)));
            assert(bag.getBySlot(0) == ItemStack(item1, 20));

            // Scenario #2, adding an item that already takes up a slot in the inventory.
            assert(bag.add(ItemStack(item1, 40)));
            assert(bag.getBySlot(0) == ItemStack(item1, 60));

            // Scenario #3, adding an item stack of 0 items.
            assert(!bag.add(ItemStack(item3, 0)));
            assert(bag.getBySlot(1) == ItemStack(null, 0));

            // Scenario #4, adding an item while the inventory is full.
            assert(bag.add(ItemStack(item2, 20))); // Fills up the last empty slot
            assert(!bag.add(ItemStack(item3, 1))); // Inventory is full, so .add returns false.

            assert(bag.getBySlot(1) == ItemStack(item2, 20));
        }

        /++
         + Gets an item stack from a certain slot.
         + 
         + Params:
         +  index = The index of the slot to get the stack of.
         + 
         + Returns:
         +  If `index` is out of bounds, an `ItemStack` with a `null` `Item` is returned.
         + 
         +  If `index` points to an empty slot, an `ItemStack` with a `null` `Item` is returned.
         + 
         +  Otherwise, the `ItemStack` at the `index`th slot is returned.
         + ++/
        @safe @nogc
        const(ItemStack) getBySlot(size_t index) nothrow const
        {
            return (index >= this._items.length) ? ItemStack(null, 0) : this._items[index];
        }
        ///
        unittest
        {
            auto bag = new Inventory(1);

            // #1, index is out of bounds.
            assert(bag.getBySlot(size_t.max).item is null);

            // #2, index points to an empty slot.
            assert(bag.getBySlot(0).item is null);

            // #3, index points to a non-empty slot.
            auto item = new Item("item1", "Test Item", "", Item.Type.Throwable, null);
            bag.add(ItemStack(item, 2));

            assert(bag.getBySlot(0) == ItemStack(item, 2));
        }

        /++
         + Removes an `ItemStack` from a specified slot in the inventory.
         +
         + Params:
         +  index = The index of the slot to remove from.
         +
         + Returns:
         +  `true` if an item stack was removed.
         +  
         +  `false` otherwise.
         + ++/
        @safe @nogc
        bool removeBySlot(size_t index) nothrow
        {
            if(index >= this._items.length)     return false;
            if(this._items[index].item is null) return false;

            this._items[index] = ItemStack(null, 0);

            // Move all of the item stacks to the left by one, so there's not a sudden Empty space inbetween items.
            if(index != this._items.length - 1)
            {
                foreach(i; index..this._items.length - 1)
                    this._items[i] = this._items[i + 1];

                //this._items[index..$ - 1] = this._items[index + 1..$];
                this._items[$ - 1] = ItemStack(null, 0);
            }

            return true;
        }
        ///
        unittest
        {
            auto bag = new Inventory(3);
            auto item1  = new Item("item1", "Test Item", "", Item.Type.Throwable, null);
            auto item2  = new Item("item2", "Test Item", "", Item.Type.Throwable, null);
            auto item3  = new Item("item3", "Test Item", "", Item.Type.Throwable, null);

            // #1, index is out of bounds.
            assert(!bag.removeBySlot(size_t.max));

            // #2, index points to a null space.
            assert(!bag.removeBySlot(0));

            // #3, index points to a valid space.
            assert(bag.add(ItemStack(item1, 1)));
            assert(bag.add(ItemStack(item2, 1)));
            assert(bag.add(ItemStack(item3, 1)));

            // Remove the middle stack, which should make the 3rd stack move to the second position.
            assert(bag.removeBySlot(1));

            assert(bag.getBySlot(0) == ItemStack(item1, 1));
            assert(bag.getBySlot(1) == ItemStack(item3, 1));
            assert(bag.getBySlot(2) == ItemStack(null,  0));
        }

        /++
         + Decrements the amount of items in a stack by a certain amount.
         + 
         + Assertions:
         +  `amount` must not be 0.
         + 
         + Notes:
         +  If the `amount` to decrement by is larger than the amount of items in the stack, then
         +  this function acts lke a call to `Inventory.removeBySlot`.
         + 
         + Params:
         +  index   = The index of the slot to decrement.
         +  amount  = How many items to remove from the stack.
         + 
         + Returns:
         +  `false` if `index` is out of bounds, or points to an empty stack.
         + 
         +  `true` otherwise.
         + ++/
        @safe @nogc
        bool decrementBySlot(size_t index, uint amount = 1) nothrow
        {
            assert(amount > 0, "Parameter 'amount' must be greater than 0.");

            // Get the item stack.
            if(index >= this._items.length)
                return false;

            auto slot = &this._items[index]; // Getting a reference, so we can modify it.
            if(slot.item is null)
                return false;

            // Remove the stack if needed
            if(amount >= slot.amount)
                return this.removeBySlot(index);

            // Otherwise, decrement the amount
            assert(amount < slot.amount);
            slot.amount -= amount;
            return true;
        }
        ///
        unittest
        {
            auto bag  = new Inventory(2);
            auto item = new Item("item", "Test Item", "", Item.Type.Throwable, null);

            bag.add(ItemStack(item, 2));

            // Scenario #1, index is out of bounds.
            assert(!bag.decrementBySlot(200));

            // Scenario #2, index is an empty stack.
            assert(!bag.decrementBySlot(1));

            // Scenario #3, the amount is decremented.
            assert(bag.getBySlot(0).amount == 2);
            assert(bag.decrementBySlot(0));
            assert(bag.getBySlot(0).amount == 1);

            // Scenario #4, the stack is removed.
            assert(bag.getBySlot(0).item == item);
            assert(bag.decrementBySlot(0));
            assert(bag.getBySlot(0).item is null);
            assert(!bag.decrementBySlot(0));
        }

        /++
         + Looks for an item with a specific ID in the inventory, and returns what slot it is in.
         + 
         + Params:
         +  id = The ID of the item to find.
         + 
         + Returns:
         +  If an item with the ID of `id` is found, the first slot found with this item in it.
         + 
         +  Otherwise, `null` is returned.
         + ++/
        @safe @nogc
        Nullable!size_t findSlotById(string id) nothrow inout
        {
            alias Size_t = typeof(return);

            foreach(i, stack; this._items)
            {
                if(stack.item is null)
                    break;

                if(stack.item.id == id)
                    return Size_t(i);
            }

            return Size_t(); // Returns null. I wish I could do Size_t(null) to make it more clear.
        }
        ///
        unittest
        {
            auto bag    = new Inventory(3);
            auto item1  = new Item("item1", "Item 1", "", Item.Type.Consumable);
            auto item2  = new Item("item2", "Item 2", "", Item.Type.Throwable);

            bag.add(ItemStack(item1, 1));
            bag.add(ItemStack(item2, 2));

            // Scenario #1, the item exists.
            auto slot = bag.findSlotById("item2");
            assert(slot == 1);

            assert(bag.getBySlot(slot).item.name == "Item 2");

            // Scenario #2, the item doesn't exist.
            assert(bag.findSlotById("Non existant item ID").isNull);
        }
    }
}

/// issue #4
unittest
{
    auto bag = new Inventory();
    bag.add(Inventory.ItemStack(new Item("i_potion_healing", "", "", Item.Type.Consumable), 1));
    bag.removeBySlot(0);
}