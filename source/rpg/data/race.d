﻿module rpg.data.race;

private
{
    import std.typecons;
    import rpg.data.stat, rpg.data.entity, rpg.util.sdlgen;
}

/++
 + Describes a race.
 + ++/
final class Race
{
    /// Default stats that can be used to create a default race, such as for testing.
    @safe
    static const(Stat[string]) defaultStats() nothrow pure
    {
        return [Entity.STAT_HEALTH_MAX: Stat(Entity.STAT_HEALTH_MAX, 20), 
                Entity.STAT_INTELLECT:  Stat(Entity.STAT_INTELLECT,  20),
                Entity.STAT_STRENGTH:   Stat(Entity.STAT_STRENGTH,   20)];
    }

    private
    {
        // Any stat name in this array *must* be defined by all races.
        static immutable string[] _requiredStats = 
        [Entity.STAT_HEALTH_MAX, Entity.STAT_INTELLECT, Entity.STAT_STRENGTH];

        string              _id;
        string              _name;
        const(Stat[string]) _baseStats;
        const(Stat[string]) _statsPerLevel;

        @trusted
        void ensureStat(string name)
        {
            import std.format    : format;
            import std.exception : enforce;

            enforce((name in this._baseStats) !is null,     format("The race '%s' must specify a base stat for '%s'", this.name, name));
            enforce((name in this._statsPerLevel) !is null, format("The race '%s' must specify a stat-per-level for '%s'", this.name, name));
        }
    }

    public
    {
        /++
         + Creates a new race.
         + 
         + Params:
         +  id              = The ID of the race.
         +  name            = The name of the race.
         +  baseStats       = The base stats of the race.
         +  statsPerLevel   = The stats gained per level.
         + ++/
        this(string id, string name, const Stat[string] baseStats, const Stat[string] statsPerLevel)
        {
            this._id            = id;
            this._name          = name;
            this._baseStats     = baseStats.dup;
            this._statsPerLevel = statsPerLevel.dup;

            foreach(stat; Race._requiredStats)
                this.ensureStat(stat);
        }

        /++
         + ++/
        @trusted
        override string toString() const
        {
            import std.format : format;

            return format("<Debug info for a Race>\n" ~
                          "ID: %s\n"                  ~
                          "Name: %s\n"                ~
                          "Base Stats: %s\n"          ~
                          "Stats Per Level: %s",

                          this.id,
                          this.name,
                          this.baseStats,
                          this.statsPerLevel);
        }

        /++
         + Returns:
         +  The ID for this race.
         + ++/
        @property @safe @nogc
        string id() nothrow inout
        {
            return this._id;
        }

        /++
         + Returns:
         +  The name of this race.
         + ++/
        @property @safe @nogc
        string name() nothrow inout
        {
            return this._name;
        }

        /++
         + Returns:
         +  The base stats of this race.
         + ++/
        @property @safe @nogc
        const(Stat[string]) baseStats() nothrow const
        {
            return this._baseStats;
        }

        /++
         + Returns:
         +  The stats that an `Entity` of this race gains per level.
         + ++/
        @property @safe @nogc
        const(Stat[string]) statsPerLevel() nothrow const
        {
            return this._statsPerLevel;
        }
    }

    //
    mixin(describeSdl("SdlBaseStats",       "baseStats",     Field!(Stat.SdlData[])("stats")));
    mixin(describeSdl("SdlStatsPerLevel",   "statsPerLevel", Field!(Stat.SdlData[])("stats")));

    mixin(describeSdl("SdlData", "race",
        Field!string            ("id"),
        Field!(Nullable!string) ("name"),
        Field!SdlBaseStats      ("baseStats"),
        Field!SdlStatsPerLevel  ("statsPerLevel")
    ));
    ///
    unittest
    {        
        auto tag = parseSource(`
        race {
            id "r_elf"
            name "Elf"

            baseStats {
                stat "healthMax" value=20
                stat "strength"  value=5
                stat "intellect" value=8
            }

            statsPerLevel {
                stat "healthMax" value=2
                stat "strength"  value=1
                stat "intellect" value=3
            }
        }
        `).getTag("race");

        auto sdl = Race.SdlData.parseTag(tag);

        assert(sdl.id   == "r_elf");
        assert(sdl.name == "Elf");

        alias S = Stat.SdlData;
        assert(sdl.baseStats.stats     == [S("healthMax", 20), S("strength", 5), S("intellect", 8)]);
        assert(sdl.statsPerLevel.stats == [S("healthMax", 2),  S("strength", 1), S("intellect", 3)]);
    }
}