﻿module rpg.data.entity;

private
{
    import rpg.data.stat, rpg.data.item, rpg.data.gear;
}

public
{
    import rpg.data.race;
}

/++
 + The base class for all entities in the game.
 + ++/
abstract class Entity
{
    /// Generates a getter property for a stat in the _stats array.
    protected static string statGetter(string statName, string funcName)
    {
        import std.format : format;
        return format(
            "/// Returns:\n"~
            "///   The entity's %s\n"~
            "@safe @property inout(Stat) %s() nothrow inout { return this._stats[\"%s\"]; }", 
            statName, funcName, statName);
    }

    private
    {
        string          _name;
        Stat[string]    _stats;
        const(Race)     _race;
        Gear            _gear;
    }

    public
    {
        // Pre-defined stats
        static immutable
        {
            ///
            string STAT_HEALTH_MAX      = "healthMax";
            ///
            string STAT_HEALTH_CURRENT  = "health";
            ///
            string STAT_STRENGTH        = "strength";
            ///
            string STAT_INTELLECT       = "intellect";
        }

        /++
         + ++/
        @safe
        this(string name, const(Race) race = new Race("race_default", "Default Race/No Race", Race.defaultStats, Race.defaultStats))
        {
            this._name  = name;
            this._race  = race;
            this._gear  = new Gear();

            // Copy stats
            foreach(k, v; race.baseStats)
                this._stats[k] = v;

            this._stats[Entity.STAT_HEALTH_CURRENT] = Stat(Entity.STAT_HEALTH_CURRENT, this.healthMax.value);
        }

        /++
         + Returns:
         +  A debug string.
         + ++/
        @trusted
        override string toString()
        {
            import std.format       : format;
            import std.algorithm    : map, joiner;

            return format(
                "<Debug info for Entity>\n"~
                "Name: %s\n\n"~
                "[Stats]%s"~
                "\n\n[Race]\n%s",

                this.name,
                this.stats.byValue.map!(s => format("\n%s: %s", s.name, s.value))
                          .joiner(" "),
                this.race);
        }

        /++
         + Called whenever the entity takes damage.
         + 
         + Inheriting classes should always call `super.onDamage`
         + 
         + Notes:
         +  If `amount` is less than `0`, it is changed to `0`
         +
         +  It is recommended that inheritng classes call `super.onDamage` near the end of the function..
         + 
         + Params:
         +  amount = The amount of damage to try and apply.
         + ++/
        @safe
        abstract void onDamage(int amount)
        {
            if(amount < 0) amount = 0;

            auto health   = (Entity.STAT_HEALTH_CURRENT in this._stats); // We need a reference to it, so we can update it's value.
            health.value -= amount;

            if(health.value < 0)
            {
                health.value = 0;
                this.onDeath();
            }
        }

        /++
         + Heals the entity for a certain amount.
         +
         + Inheriting classes should always call `super.onHeal`
         +
         + Notes:
         +  If `amount` is less than `0`, it is changed to `0`.
         +
         +  The entity won't be healed for more than it's max health.
         +
         + Params:
         +  amount = The amount to heal the entity by.
         +
         + Returns:
         +  How much HP was actually healed.
         + ++/
        @safe
        abstract uint onHeal(int amount)
        {
            if(amount < 0) amount = 0;

            auto health = (Entity.STAT_HEALTH_CURRENT in this._stats);
            assert(health !is null);
            
            auto oldValue = *health;
            health.value += amount;
            if(health.value > this.healthMax.value)
                health.value = this.healthMax.value;

            return (health.value - oldValue.value);
        }

        /++
         + Called whenever the entity dies.
         + ++/
        @safe
        abstract void onDeath();

        mixin(Entity.statGetter(Entity.STAT_HEALTH_CURRENT, "healthCurrent"));
        mixin(Entity.statGetter(Entity.STAT_HEALTH_MAX,     "healthMax"));
        mixin(Entity.statGetter(Entity.STAT_STRENGTH,       "strength"));
        mixin(Entity.statGetter(Entity.STAT_INTELLECT,      "intellect"));

        /++
         + Returns:
         +  An associative-array of the entity's stats.
         + ++/
        @property @safe @nogc
        final inout(Stat[string]) stats() nothrow inout
        {
            return this._stats;
        }

        /++
         + Returns:
         +  The name of the entity.
         + ++/
        @property @safe @nogc
        final string name() nothrow inout
        {
            return this._name;
        }

        /++
         + Returns:
         +  This entity's `Race`.
         + ++/
        @property @safe @nogc
        final const(Race) race() nothrow const
        {
            return this._race;
        }

        /++
         + Returns:
         +  Returns the `Gear` that the entity has equipped.
         + ++/
        @property @safe @nogc
        final inout(Gear) gear() nothrow inout
        out(gear)
        {
            assert(gear !is null, "Don't forget to initialise this, future me.");
        }
        body
        {
            return this._gear;
        }
    }
}

/++
 + Contains information about the player entity.
 + ++/
final class EntityPlayer : Entity
{
    import rpg.data.inventory : Inventory;
    import rpg.game           : Game;
    import rpg.command        : Command, CommandHandler;

    // Constants
    enum INVENTORY_SIZE = 32;

    private
    {
        Inventory _inventory;

        @trusted
        void commandBagHelp(Command.Args args)
        {
            import std.stdio      : writeln, writefln;
            import std.algorithm  : each;

            writeln("Avaliable commands:");
            args.handler.commands.byValue.each!(c => writefln("\t%s - %s", c.name, c.description));
        }

        @trusted
        void commandBagOpen(Command.Args args)
        {
            import std.stdio      : writeln, writefln;
            import rpg.util.input : Input;

            bool isOpen = true;
            void commandBagClose(Command.Args args)
            {
                isOpen = false;
            }

            // Create the command handler, and add all commands.
            auto handler = new CommandHandler();
            handler.register(Command("exit",    "Closes the bag.",                                      &commandBagClose));
            handler.register(Command("info",    "Inspects a slot in the inventory.",                    &commandBagInfo));
            handler.register(Command("help",    "Displays this list of commands.",                      &commandBagHelp));
            handler.register(Command("use",     "Uses an item in the bag.",                             &commandBagUse));
            handler.register(Command("update",  "Shows an up-to-date display of the bag's contents.",   &commandBagUpdate));

            // Display the bag + commands.
            writeln(this.bag);
            handler.processCommandString("help", args.game);

            // While the bag is open, keep taking commands from the player.
            while(isOpen)
            {
                try
                {
                    auto command  = Input.read!string("\nCommand");
                    auto wasValid = handler.processCommandString(command, args.game);

                    if(!wasValid)
                        writefln("<Invalid command '%s'>", command);
                }
                catch(Exception ex)
                {
                    writefln("<ERROR: %s>", ex.msg);
                }
            }
        }

        @safe
        void commandBagUpdate(Command.Args args)
        {
            import std.stdio : writeln;
            writeln(this.bag);
        }

        @safe
        void commandBagInfo(Command.Args args)
        {
            import std.stdio : writeln, writefln;
            import std.conv  : to, ConvException;

            immutable usage = "<USAGE: info [slot number] | e.g. info 2>";

            // Make sure the user provided an argument.
            if(args.args.length == 0)
            {
                writeln(usage);
                throw new Exception("Expected at least one argument.");
            }

            // Convert the argument to a number, then get the slot at that number.
            size_t index;
            try
                index = args.argAs!size_t(0);
            catch(ConvException ex)
            {
                writeln(usage);
                throw ex;
            }

            auto stack = this.bag.getBySlot(index);
            writefln("\n[Information about slot #%s]", index);

            // If the number is out of bounds/the slot is empty, just display that the slot is empty.
            if(stack.item is null)
            {
                writeln("\tEmpty Slot");
                return;
            }

            // Write out the information about the slot.
            writefln("\tName: %s\n"
                    ~"\tDescription: %s\n"
                    ~"\tType: %s\n"
                    ~"\tAmount: %s",

                    stack.item.name,
                    stack.item.description,
                    stack.item.type,
                    stack.amount);
        }

        @trusted
        void commandBagUse(Command.Args args)
        {
            import std.stdio : writeln, writefln;
            import std.conv  : to, ConvException;

            immutable usage = "<USAGE: use [slot number] | e.g. use 2>";

            if(args.args.length == 0)
            {
                writeln(usage);
                throw new Exception("Expected at least one argument.");
            }

            try
            {
                auto index = args.argAs!size_t(0);
                auto stack = this.bag.getBySlot(index);

                args.game.environment.useItem(stack.item, this.bag, this);
            }
            catch(ConvException ex)
            {
                writeln(usage);
                throw ex;
            }
        }
    }

    public
    {
        /++
         + ++/
        @safe
        this(string name, Game game)
        {
            super(name);

            // Setup variables
            this._inventory = new Inventory(EntityPlayer.INVENTORY_SIZE);

            // Register commands
            game.commands.register(Command("bag",  "Opens the player's inventory", &this.commandBagOpen));
        }

        override
        {
            @safe
            void onDeath()
            {

            }

            @safe
            void onDamage(int amount)
            {
                super.onDamage(amount);
            }

            @safe
            uint onHeal(int amount)
            {
                import std.stdio : writefln;
                auto healed = super.onHeal(amount);

                writefln("[You healed for %s HP]", healed);

                return healed;
            }
        }

        /++
         + Returns:
         +  The player's `Inventory`.
         + ++/
        @property @safe @nogc
        inout(Inventory) bag() nothrow inout
        {
            return this._inventory;
        }
    }
}

/++
 + Contains information about an enemy entity.
 + ++/
final class EntityEnemy : Entity
{
    import rpg.util.sdlgen, rpg.data.loot;

    // The entire enemy tag
    mixin(describeSdl("SdlData", "enemy",
        Field!string                        ("id"),
        Field!string                        ("raceId"),
        Field!(Nullable!string)             ("name"),
        Field!(Nullable!string)             ("description"),
        Field!(Nullable!(LootTable.SdlData))("loot"),
        Field!(Nullable!(Gear.SdlData))     ("gear")
    ));

    /// Contains information about the enemy.
    struct Info
    {
        /// The enemy's ID.
        string    id;

        /// The name of the enemy.
        string    name;

        /// The description of the enemy.
        string    description;

        /// The race of the enemy.
        Race      race;

        /// The loot table for the enemy.
        LootTable loot;

        /// The entity's equipped gear.
        Gear      gear;

        ///
        @trusted
        string toString() const
        {
            import std.format : format;

            return format("<Debug Information for an Enemy>\n"~
                          "ID: %s\n"~
                          "Name: %s\n"~
                          "Description: %s\n"~
                          "\n%s\n"~
                          "\n%s\n"~
                          "\n%s",
                          this.id, this.name, this.description, this.race, this.loot, this.gear);
        }

        // Because some fields such as 'raceId' need to be resolved into an actual type before
        // it can be used to create an EntityEnemy, this struct is provided
        // as a way for the 'Loader' to resolve everything so that the EntityEnemy
        // doesn't have to manually make calls to 'Loader'.
    }

    private
    {
    }

    public
    {
        ///
        @safe
        this(string id, string name, string description, const(Race) race)
        {
            super(name, race);
        }

        ///
        @safe
        this(const Info info)
        {
            this(info.id, info.name, info.description, info.race);
        }
    }
}