﻿module rpg.command;

private
{
    import std.exception : basicExceptionCtors, enforce;
    import std.typecons  : nullable, Nullable;
    import std.traits    : isNumeric;

    import rpg.game : Game;
}

/++
 + Describes a command that can be registered in the `CommandHandler`.
 + ++/
struct Command
{
    /// The type of function that can be used as a command.
    alias Action = void delegate(Command.Args args) @safe;

    /++
     + Information passed to the actions of commands.
     + ++/
    struct Args
    {
        /// The command the user passed through, always lower-case.
        string command;

        /// Arguments given to the command.
        /// 
        /// For example, "give 0 2" would produce the args ["0", "2"] for the 'give' command.
        /// 
        /// Use the `Args.argAs` function to easily get an argument as a certain type.
        string[] args;

        /++
         + The state of the `Game`.
         + ++/
        Game game;

        /++
         + The `CommandHandler` calling the function.
         + 
         + This should never be set manually.
         + ++/
        CommandHandler handler = null;

        /++
         + Converts an argument into a certain type.
         + 
         + Params:
         +  T     = The type to convert the argument to.
         +  index = The index of the argument to use.
         + 
         + Returns:
         +  The `index`th argument converted to a `T`.
         + ++/
        @trusted
        T argAs(T)(size_t index)
        if(isNumeric!T || is(T == bool) || is(T == string))
        {
            import std.exception : enforce;
            import std.format    : format;
            import std.conv      : to, ConvException;

            // Get the argument
            assert(index < this.args.length, format("Attempted to index argument '%s', which is out of bounds.", index));
            auto text = this.args[index];

            static if(is(T == string))
                return text;
            else try
                return text.to!T;
            catch(ConvException ex)
                throw new ConvException(format("Expected argument #%s('%s') to be convertable to a '%s'",
                                               index, text, T.stringof));
        }
        ///
        unittest
        {
            import std.math : approxEqual;

            Args args = Args(null, ["true", "This is text", "20", "20.1"]);

            assert(args.argAs!bool(0)   == true);
            assert(args.argAs!string(1) == "This is text");
            assert(args.argAs!int(2)    == 20);
            assert(args.argAs!float(3).approxEqual(20.1));
        }
    }

    public
    {
        /// The name of the command.
        string name;

        /// The description of the command.
        string description;

        /// The action of the command.
        Action action;
    }
}

/++
 + Handles processing command strings to then pass onto registered commands.
 + ++/
final class CommandHandler
{
    private
    {
        Command[string] _commands;
    }

    public
    {
        /++
         + Registers a command with the handler.
         + 
         + Side Effects:
         +  The name of the `command` is stored in all lower-case, so casing of command names has no effect.
         + 
         + Exceptions:
         +  `CommandException` if `command` shares the same name as another previously-registered command.
         + 
         +  `Exception` if `command.action` is `null`.
         + 
         + Params:
         +  command = The command to register.
         + ++/
        @safe
        void register(Command command)
        {
            import std.format : format;
            import std.uni    : toLower;

            command.name = command.name.toLower;
            enforce!CommandException((command.name in this._commands) is null,
                                     format("Command '%s' already exists.", command.name));

            enforce(command.action !is null, format("Action for command '%s' is null."));

            this._commands[command.name] = command;
        }
        ///
        unittest
        {
            void myAction(Command.Args args)
            {
            }

            auto handler = new CommandHandler();
            handler.register(Command("My Action", "It does stuff", &myAction));

            assert(!handler.get("My Action").isNull);
        }

        /++
         + Gets a command with a specific name.
         + 
         + Params:
         +  name = The name of the command to get.
         + 
         + Returns:
         +  `null` if a command with `name` doesn't exist.
         + 
         +  Otherwise, the `Command` called 'name'.
         + ++/
        @safe
        Nullable!Command get(string name) inout
        {
            import std.uni : toLower;

            auto pointer = (name.toLower in this._commands);
            Nullable!Command command;

            if(pointer !is null)
                command = *pointer;

            return command;
        }
        ///
        unittest
        {
            void myAction(Command.Args args)
            {
            }
            
            auto handler = new CommandHandler();
            handler.register(Command("My Action", "It does stuff", &myAction));
            
            assert(!handler.get("My Action").isNull);
        }

        /++
         + Processes a command string, and attempts to call the corresponding `Command`.
         + 
         + For example, the command string `"look up"` would call the 'look' `Command` (if it was registered) with
         + the arguments `["up"]`.
         + 
         + Params:
         +  command = The command string.
         +  player  = A player `Entity`, generally this will be the main player entity for the game.
         + 
         + Returns:
         +  `true` if a command was called.
         +  `false` otherwise.
         + ++/
        @safe
        bool processCommandString(string command, Game game)
        {
            import std.algorithm : splitter;
            import std.array     : array;
            import std.uni       : toLower;

            // Split the command by space
            auto split = command.splitter(" ").array;
            if(split.length == 0)
                return false;

            // Get the command to call, and call it.
            auto toCall = this.get(split[0]);
            if(toCall.isNull)
                return false;

            // Setup the command argument, and call the action.
            auto args       = Command.Args();
            args.command    = split[0].toLower;
            args.args       = (split.length == 1) ? null : split[1..$];
            args.game       = game;
            args.handler    = this;

            toCall.action(args);

            return true;
        }
        ///
        unittest
        {
            bool flag = false;
            void myAction(Command.Args args)
            {
                flag = (args.args[0] == "SetFlag");
            }

            auto handler = new CommandHandler();
            handler.register(Command("flag", "Sets a flag if the first argument is 'SetFlag'", &myAction));

            // Scenario #1: THe command string is empty, so nothing is called.
            assert(!handler.processCommandString("", null));

            // Scenario #2: The command doesn't exist, so nothing is called.
            assert(!handler.processCommandString("superCheats infinite life", null));

            // Scenario #3: The command exists, so it gets called.
            assert(!flag);
            assert(handler.processCommandString("flag SetFlag", null));
            assert(flag);
        }

        /++
         + Unregisters a command using it's name.
         + 
         + Params:
         +  name = The name of the `Command` to unregister.
         + 
         + Returns:
         +  `true` if the command called `name` was removed.
         + 
         +  `false` otherwise.
         + ++/
        @safe
        bool unregister(string name)
        {
            return this._commands.remove(name);
        }
        ///
        unittest
        {
            void command(Command.Args args) {}

            auto handler = new CommandHandler();
            handler.register(Command("test", "", &command));

            assert( handler.unregister("test"));
            assert(!handler.unregister("test"));
            assert(handler.get("test").isNull);
        }

        /++
         + Returns:
         +  A `const` reference to the array of registered commands.
         + ++/
        @safe @nogc
        const(Command[string]) commands() nothrow const
        {
            return this._commands;
        }
    }
}

/++
 + Thrown whenever something regarding commands has gone wrong.
 + ++/
class CommandException : Exception
{
    mixin basicExceptionCtors;
}